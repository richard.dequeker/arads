﻿using Microsoft.AspNetCore.Mvc;
using SharingService.Data;
using System.Threading.Tasks;

namespace SharingService.Controllers
{
    [Route("api/apptoken")]
    [ApiController]
    public class AppTokenController : ControllerBase
    {
        private SpatialAnchorsTokenService tokenService;

        public AppTokenController(SpatialAnchorsTokenService tokenService)
        {
            this.tokenService = tokenService;
        }

        // GET api/apptoken
        [HttpGet]
        public Task<string> GetAsync()
        {

            return this.tokenService.RequestToken();
        }
    }
}
