using Microsoft.AspNetCore.Mvc;
using SharingService.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace SharingService.Controllers
{
    [Route("api/anchors")]
    [ApiController]
    public class AnchorsController : ControllerBase
    {
        private readonly IAnchorKeyCache anchorKeyCache;

        public AnchorsController(IAnchorKeyCache anchorKeyCache)
        {
            this.anchorKeyCache = anchorKeyCache;
        }

        // GET api/anchors/:id pas le temps !!!
        [HttpGet("{anchorId}")]
        public async Task<ActionResult<string>> GetTextAsync(string anchorId)
        {
            // Get the text if present
            try
            {
                return await this.anchorKeyCache.GetAnchorTextAsync(anchorId);
            }
            catch (KeyNotFoundException)
            {
                return this.NotFound();
            }
        }

        // GET api/anchors
        [HttpGet]
        public async Task<ActionResult<List<string>>> GetAllAsync()
        {
            // Get the key if present
            try
            {
                return await this.anchorKeyCache.GetAnchorKeysAsync();
            }
            catch(Exception e)
            {
                return this.NotFound(e.Message);
            }
        }

        // POST api/anchors
        [HttpPost]
        public async Task<ActionResult<string>> PostAsync()
        {
            string anchorKey;
            using (StreamReader reader = new StreamReader(this.Request.Body, Encoding.UTF8))
            {
                anchorKey = await reader.ReadToEndAsync();
            }

            if (string.IsNullOrWhiteSpace(anchorKey))
            {
                return this.BadRequest();
            }

            // Set the key and return the anchor number
            return await this.anchorKeyCache.SetAnchorKeyAsync(anchorKey);
        }

        // POST api/anchors/:text
        [HttpPost("{anchorText}")]
        public async Task<ActionResult<string>> PostTextAsync(string anchorText)
        {
            using (StreamReader reader = new StreamReader(this.Request.Body, Encoding.UTF8))
            {
                anchorText = await reader.ReadToEndAsync();
            }

            if (string.IsNullOrWhiteSpace(anchorText))
            {
                return this.BadRequest();
            }

            await this.anchorKeyCache.SetAnchorTextAsync(anchorText);
            return anchorText;
        }
    }
}
