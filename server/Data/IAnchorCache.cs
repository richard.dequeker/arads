
using System.Threading.Tasks;
using System.Collections.Generic;

namespace SharingService.Data
{

    public interface IAnchorKeyCache
    {

        Task<bool> ContainsAsync(long anchorId);

        Task<string> GetAnchorTextAsync(string anchorId);

        Task<string> GetLastAnchorKeyAsync();

        Task<List<string>> GetAnchorKeysAsync();

        Task<string> SetAnchorKeyAsync(string anchorKey);

        Task<string> SetAnchorTextAsync(string anchorText);
    }
}
