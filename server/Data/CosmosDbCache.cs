using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SharingService.Data
{
    internal class AnchorCacheEntity : TableEntity
    {
        public AnchorCacheEntity() { }

        public AnchorCacheEntity(string anchorId)
        {
            this.PartitionKey = anchorId;
            this.RowKey = anchorId;
        }

        public string AnchorKey { get; set; }

        public string AnchorText { get; set; }
    }


    internal class CosmosDbCache : IAnchorKeyCache
    {

        private const int partitionSize = 500;

        private readonly CloudTable dbCache;

        private long lastAnchorNumberIndex = -1;

        ManualResetEventSlim initialized = new ManualResetEventSlim();
        ManualResetEventSlim initializing = new ManualResetEventSlim();

        private async Task InitializeAsync()
        {
            if (!this.initialized.Wait(0))
            {
                if (!this.initializing.Wait(0))
                {
                    this.initializing.Set();
                    await this.dbCache.CreateIfNotExistsAsync();
                    this.initialized.Set();
                }
                this.initialized.Wait();
            }
        }

        public CosmosDbCache(string storageConnectionString)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(storageConnectionString);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            this.dbCache = tableClient.GetTableReference("AnchorCache");
        }

        public async Task<bool> ContainsAsync(long anchorId)
        {
            await this.InitializeAsync();
            TableResult result = await this.dbCache.ExecuteAsync(TableOperation.Retrieve<AnchorCacheEntity>((anchorId / CosmosDbCache.partitionSize).ToString(), anchorId.ToString()));
            AnchorCacheEntity anchorEntity = result.Result as AnchorCacheEntity;
            return anchorEntity != null;
        }


        public async Task<string> GetAnchorKeyAsync(string anchorId)
        {
            await this.InitializeAsync();
            TableResult result = await this.dbCache.ExecuteAsync(TableOperation.Retrieve<AnchorCacheEntity>(anchorId, anchorId));
            AnchorCacheEntity anchorEntity = result.Result as AnchorCacheEntity;
            if (anchorEntity != null)
            {
                return anchorEntity.AnchorKey;
            }
            throw new KeyNotFoundException($"The {nameof(anchorId)} {anchorId} could not be found.");
        }

        public async Task<string> GetAnchorTextAsync(string anchorId)
        {
            await this.InitializeAsync();
            TableResult result = await this.dbCache.ExecuteAsync(TableOperation.Retrieve<AnchorCacheEntity>(anchorId, anchorId));
            AnchorCacheEntity anchorEntity = result.Result as AnchorCacheEntity;
            if (anchorEntity != null)
            {
                return anchorEntity.AnchorText;
            }
            throw new KeyNotFoundException($"The {nameof(anchorId)} {anchorId} could not be found.");
        }

        public async Task<AnchorCacheEntity> GetLastAnchorAsync()
        {
            await this.InitializeAsync();
            List<AnchorCacheEntity> results = new List<AnchorCacheEntity>();
            TableQuery<AnchorCacheEntity> tableQuery = new TableQuery<AnchorCacheEntity>();
            TableQuerySegment<AnchorCacheEntity> previousSegment = null;
            while (previousSegment == null || previousSegment.ContinuationToken != null)
            {
                TableQuerySegment<AnchorCacheEntity> currentSegment = await this.dbCache.ExecuteQuerySegmentedAsync<AnchorCacheEntity>(tableQuery, previousSegment?.ContinuationToken);
                previousSegment = currentSegment;
                results.AddRange(previousSegment.Results);
            }
            return results.OrderByDescending(x => x.Timestamp).DefaultIfEmpty(null).First();
        }

        public async Task<string> GetLastAnchorKeyAsync()
        {
            return (await this.GetLastAnchorAsync())?.AnchorKey;
        }

        public async Task<List<string>> GetAnchorKeysAsync()
        {
            await this.InitializeAsync();

            List<AnchorCacheEntity> results = new List<AnchorCacheEntity>();
            TableQuery<AnchorCacheEntity> tableQuery = new TableQuery<AnchorCacheEntity>();
            TableQuerySegment<AnchorCacheEntity> previousSegment = null;
            while (previousSegment == null || previousSegment.ContinuationToken != null)
            {
                TableQuerySegment<AnchorCacheEntity> currentSegment = await this.dbCache.ExecuteQuerySegmentedAsync<AnchorCacheEntity>(tableQuery, previousSegment?.ContinuationToken);
                previousSegment = currentSegment;
                results.AddRange(previousSegment.Results);
            }
            List<string> ids = new List<string>();
            for (int i = 0; i < results.Count; i++)
            {
                ids.Add(results[i].AnchorKey);
            }
            return ids;
        }

        public async Task<string> SetAnchorKeyAsync(string anchorKey)
        {
            await this.InitializeAsync();
            AnchorCacheEntity anchorEntity = new AnchorCacheEntity(anchorKey)
            {
                AnchorKey = anchorKey,
                AnchorText = ""
            };
            await this.dbCache.ExecuteAsync(TableOperation.Insert(anchorEntity));
            return anchorKey;
        }

        public async Task<string> SetAnchorTextAsync(string anchorText)
        {
            await this.InitializeAsync();
            AnchorCacheEntity anchorEntity = await GetLastAnchorAsync();
            anchorEntity.AnchorText = anchorText;
            await this.dbCache.ExecuteAsync(TableOperation.Merge(anchorEntity));
            return "ok";
        }
    }
}