
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SharingService.Data
{
    internal class MemoryAnchorCache
    {

        private static readonly MemoryCacheEntryOptions entryCacheOptions = new MemoryCacheEntryOptions
        {
            SlidingExpiration = TimeSpan.FromHours(48),
        };


        private readonly MemoryCache memoryCache = new MemoryCache(new MemoryCacheOptions());


        private long anchorNumberIndex = -1;


        public Task<bool> ContainsAsync(long anchorId)
        {
            return Task.FromResult(this.memoryCache.TryGetValue(anchorId, out _));
        }

        public Task<string> GetAnchorKeyAsync(long anchorId)
        {
            if (this.memoryCache.TryGetValue(anchorId, out string anchorKey))
            {
                return Task.FromResult(anchorKey);
            }

            return Task.FromException<string>(new KeyNotFoundException($"The {nameof(anchorId)} {anchorId} could not be found."));
        }


        public Task<string> GetLastAnchorKeyAsync()
        {
            if (this.anchorNumberIndex >= 0 && this.memoryCache.TryGetValue(this.anchorNumberIndex, out string anchorKey))
            {
                return Task.FromResult(anchorKey);
            }

            return Task.FromResult<string>(null);
        }


        public async Task<List<string>> GetAnchorKeysAsync()
        {
            return new List<string>();
        }


        public Task<long> SetAnchorKeyAsync(string anchorKey)
        {
            if (this.anchorNumberIndex == long.MaxValue)
            {
                this.anchorNumberIndex = -1;
            }

            long newAnchorNumberIndex = ++this.anchorNumberIndex;
            this.memoryCache.Set(newAnchorNumberIndex, anchorKey, entryCacheOptions);

            return Task.FromResult(newAnchorNumberIndex);
        }
    }
}
