using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

using UnityEngine.XR.ARFoundation;


public abstract class ARAdsInputBase : MonoBehaviour
{
    ARRaycastManager arRaycastManager;

    public virtual void Start()
    {
        arRaycastManager = FindObjectOfType<ARRaycastManager>();
        if (arRaycastManager == null)
        {
            Debug.Log("Missing ARRaycastManager in scene");
        }

    }

    public virtual void Update()
    {
        TriggerInteractions();
    }

    private void TriggerInteractions()
    {

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
            {
                return;
            }

            OnTouchInteraction(touch);
        }
    }
    protected virtual void OnTouchInteraction(Touch touch)
    {
        if (touch.phase == TouchPhase.Ended)
        {
            OnTouchInteractionEnded(touch);
        }
    }

    protected virtual void OnTouchInteractionEnded(Touch touch)
    {
        List<ARRaycastHit> aRRaycastHits = new List<ARRaycastHit>();
        if(arRaycastManager.Raycast(touch.position, aRRaycastHits) && aRRaycastHits.Count > 0)
        {
            ARRaycastHit hit = aRRaycastHits[0];
            OnSelectObjectInteraction(hit.pose.position, hit);
        }
    }

    protected virtual void OnSelectInteraction()
    {
    }

    protected virtual void OnSelectObjectInteraction(Vector3 hitPoint, object target)
    {
    }

}
