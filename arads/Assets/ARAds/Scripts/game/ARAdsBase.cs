using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Microsoft.Azure.SpatialAnchors;
using Microsoft.Azure.SpatialAnchors.Unity;
using TMPro;

public abstract class ARAdsBase : ARAdsInputBase
{
    private Task advanceDemoTask = null;
    protected bool isErrorActive = false;
    protected Text feedbackBox;
    protected readonly List<string> anchorIdsToLocate = new List<string>();
    protected AnchorLocateCriteria anchorLocateCriteria = null;
    protected CloudSpatialAnchor currentCloudAnchor;
    protected CloudSpatialAnchorWatcher currentWatcher;
    protected GameObject spawnedObject = null;
    protected Material spawnedObjectMat = null;
    protected List<GameObject> spawnedObjects = new List<GameObject>();
    public AnchorAPI anchorAPI = new AnchorAPI();
    protected readonly List<string> localAnchorIds = new List<string>();


    [SerializeField]
    [Tooltip("The prefab used to represent an anchored object.")]
    private GameObject anchoredObjectPrefab = null;

    [SerializeField]
    [Tooltip("SpatialAnchorManager instance to use for this demo. This is required.")]
    private SpatialAnchorManager cloudManager = null;

    [SerializeField]
    [Tooltip("The base URL for the example sharing service.")]
    private string baseSharingUrl = "";

    public virtual bool SanityCheckAccessConfiguration()
    {
        if (string.IsNullOrWhiteSpace(CloudManager.SpatialAnchorsAccountId) || string.IsNullOrWhiteSpace(CloudManager.SpatialAnchorsAccountKey))
        {
            return false;
        }

        return true;
    }

    public override void Start()
    {
        feedbackBox = UIPicker.Instance.GetFeedbackText();
        if (feedbackBox == null)
        {
            Destroy(this);
            return;
        }
        if (CloudManager == null)
        {
            feedbackBox.text = $"{nameof(CloudManager)} reference has not been set";
            return;
        }
        if (!SanityCheckAccessConfiguration())
        {
            feedbackBox.text = $"AccountId and Account key must be set on {nameof(SpatialAnchorManager)}";
        }
        if (AnchoredObjectPrefab == null)
        {
            feedbackBox.text = "CreationTarget must be set on script.";
            return;
        }

        if (string.IsNullOrEmpty(BaseSharingUrl))
        {
            feedbackBox.text = $"Need to set {nameof(BaseSharingUrl)}.";
        }
        else
        {
            Uri result;
            if (!Uri.TryCreate(BaseSharingUrl, UriKind.Absolute, out result))
            {
                feedbackBox.text = $"{nameof(BaseSharingUrl)} is not a valid url";
                return;
            }
            else
            {
                BaseSharingUrl = $"{result.Scheme}://{result.Host}/api/anchors";
            }
        }
        CloudManager.SessionUpdated += CloudManager_SessionUpdated;
        CloudManager.AnchorLocated += CloudManager_AnchorLocated;
        CloudManager.LocateAnchorsCompleted += CloudManager_LocateAnchorsCompleted;
        CloudManager.LogDebug += CloudManager_LogDebug;
        CloudManager.Error += CloudManager_Error;
        anchorLocateCriteria = new AnchorLocateCriteria();
        anchorAPI.WatchKeys(BaseSharingUrl);
        base.Start();
    }


    public abstract Task AdvanceDemoAsync();

    public async void AdvanceDemo()
    {
        try
        {
            await AdvanceDemoAsync();
        }
        catch (Exception ex)
        {
            feedbackBox.text = ex.Message;
        }
    }

    public async void ReturnToLauncher()
    {
        if (advanceDemoTask != null) { await advanceDemoTask; }
        SceneManager.LoadScene(0);
    }

    protected virtual void CleanupSpawnedObjects()
    {
        if (spawnedObject != null)
        {
            Destroy(spawnedObject);
            spawnedObject = null;
        }

        if (spawnedObjectMat != null)
        {
            Destroy(spawnedObjectMat);
            spawnedObjectMat = null;
        }
    }

    protected CloudSpatialAnchorWatcher CreateWatcher()
    {
        if ((CloudManager != null) && (CloudManager.Session != null))
        {
            return CloudManager.Session.CreateWatcher(anchorLocateCriteria);
        }
        else
        {
            return null;
        }
    }

    protected void SetAnchorIdsToLocate(IEnumerable<string> anchorIds)
    {
        if (anchorIds == null)
        {
            throw new ArgumentNullException(nameof(anchorIds));
        }
        anchorIdsToLocate.Clear();
        anchorIdsToLocate.AddRange(anchorIds);
        anchorLocateCriteria.Identifiers = anchorIdsToLocate.ToArray();
        
    }

    protected abstract bool IsPlacingObject();

    protected virtual void MoveAnchoredObject(GameObject objectToMove, Vector3 worldPos, Quaternion worldRot, CloudSpatialAnchor cloudSpatialAnchor = null)
    {
        CloudNativeAnchor cna = spawnedObject.GetComponent<CloudNativeAnchor>();
        if (cloudSpatialAnchor != null)
        {
            cna.CloudToNative(cloudSpatialAnchor);
        }
        else
        {
            cna.SetPose(worldPos, worldRot);
        }
    }

    protected virtual void OnCloudAnchorLocated(AnchorLocatedEventArgs args) {}

    protected virtual void OnCloudLocateAnchorsCompleted(LocateAnchorsCompletedEventArgs args) {}

    protected virtual void OnCloudSessionUpdated() {}

    protected virtual void OnSaveCloudAnchorFailed(Exception exception)
    {
        isErrorActive = true;
        UnityDispatcher.InvokeOnAppThread(() => this.feedbackBox.text = string.Format("Error: {0}", exception.ToString()));
    }

    protected virtual Task OnSaveCloudAnchorSuccessfulAsync()
    {
        return Task.CompletedTask;
    }

    protected override void OnSelectInteraction()
    {
        base.OnSelectInteraction();
    }

    protected override void OnSelectObjectInteraction(Vector3 hitPoint, object target)
    {
        if (IsPlacingObject())
        {
            Quaternion rotation = Quaternion.AngleAxis(0, Vector3.up);

            SpawnOrMoveCurrentAnchoredObject(hitPoint, rotation);
        }
    }

    protected override void OnTouchInteraction(Touch touch)
    {
        if (IsPlacingObject())
        {
            base.OnTouchInteraction(touch);
        }
    }

    protected virtual async Task SaveCurrentObjectAnchorToCloudAsync()
    {
        CloudNativeAnchor cna = spawnedObject.GetComponent<CloudNativeAnchor>();

        if (cna.CloudAnchor == null) { cna.NativeToCloud(); }

        CloudSpatialAnchor cloudAnchor = cna.CloudAnchor;

        cloudAnchor.Expiration = DateTimeOffset.Now.AddDays(7);

        while (!CloudManager.IsReadyForCreate)
        {
            await Task.Delay(330);
            float createProgress = CloudManager.SessionStatus.RecommendedForCreateProgress;
            feedbackBox.text = $"Move your device to capture more environment data: {createProgress:0%}";
        }

        bool success = false;

        feedbackBox.text = "Saving...";

        try
        {
            await CloudManager.CreateAnchorAsync(cloudAnchor);

            currentCloudAnchor = cloudAnchor;

            success = currentCloudAnchor != null;

            if (success && !isErrorActive)
            {
                await OnSaveCloudAnchorSuccessfulAsync();
            }
            else
            {
                OnSaveCloudAnchorFailed(new Exception("Failed to save"));
            }
        }
        catch (Exception ex)
        {
            OnSaveCloudAnchorFailed(ex);
        }
    }

    protected virtual GameObject SpawnNewAnchoredObject(Vector3 worldPos, Quaternion worldRot)
    {
        GameObject newGameObject = GameObject.Instantiate(AnchoredObjectPrefab, worldPos, worldRot);

        newGameObject.AddComponent<CloudNativeAnchor>();

        return newGameObject;
    }

    protected virtual GameObject SpawnNewAnchoredObject(Vector3 worldPos, Quaternion worldRot, CloudSpatialAnchor cloudSpatialAnchor)
    {
        GameObject newGameObject = SpawnNewAnchoredObject(worldPos, worldRot);

        if (cloudSpatialAnchor != null)
        {
            CloudNativeAnchor cloudNativeAnchor = newGameObject.GetComponent<CloudNativeAnchor>();
            cloudNativeAnchor.CloudToNative(cloudSpatialAnchor);
        }
        return newGameObject;
    }

    protected virtual void SpawnOrMoveCurrentAnchoredObject(Vector3 worldPos, Quaternion worldRot)
    {
        if (spawnedObject == null)
        {
            spawnedObject = SpawnNewAnchoredObject(worldPos, worldRot, currentCloudAnchor);
            spawnedObjectMat = spawnedObject.GetComponent<MeshRenderer>().material;
        }
        else
        {
            MoveAnchoredObject(spawnedObject, worldPos, worldRot, currentCloudAnchor);
        }
    }

    protected virtual void AttachText(string text)
    {
        if (spawnedObject != null)
        {
            TextMeshPro textMeshFront = spawnedObject.transform.GetChild(0).GetComponent<TextMeshPro>();
            textMeshFront.text = text;
            TextMeshPro textMeshBack = spawnedObject.transform.GetChild(1).GetComponent<TextMeshPro>();
            textMeshBack.text = text;
        }
    }

    private void CloudManager_AnchorLocated(object sender, AnchorLocatedEventArgs args)
    {
        Debug.LogFormat("Anchor recognized as a possible anchor {0} {1}", args.Identifier, args.Status);
        if (args.Status == LocateAnchorStatus.Located)
        {
            OnCloudAnchorLocated(args);
        }
    }

    private void CloudManager_LocateAnchorsCompleted(object sender, LocateAnchorsCompletedEventArgs args)
    {
        OnCloudLocateAnchorsCompleted(args);
    }

    private void CloudManager_SessionUpdated(object sender, SessionUpdatedEventArgs args)
    {
        OnCloudSessionUpdated();
    }

    private void CloudManager_Error(object sender, SessionErrorEventArgs args)
    {
        isErrorActive = true;
        Debug.Log(args.ErrorMessage);

        UnityDispatcher.InvokeOnAppThread(() => this.feedbackBox.text = string.Format("Error: {0}", args.ErrorMessage));
    }

    private void CloudManager_LogDebug(object sender, OnLogDebugEventArgs args)
    {
        Debug.Log(args.Message);
    }

    public GameObject AnchoredObjectPrefab { get { return anchoredObjectPrefab; } }

    public SpatialAnchorManager CloudManager { get { return cloudManager; } }

    public string BaseSharingUrl { get => baseSharingUrl; set => baseSharingUrl = value; }

}
