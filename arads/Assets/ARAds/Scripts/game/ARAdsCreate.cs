﻿using Microsoft.Azure.SpatialAnchors.Unity;
using System;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ARAdsCreate : ARAdsBase
{
    internal enum AppState
    {
        CreateSession = 0,
        StartSession,
        CreateLocalAnchor,
        SaveCloudAnchor,
        SavingCloudAnchor,
        StopSession,
    }

    protected InputField inputField;

    private AppState _currentAppState = AppState.CreateSession;

    AppState currentAppState
    {
        get
        {
            return _currentAppState;
        }
        set
        {
            if (_currentAppState != value)
            {
                _currentAppState = value;
            }
        }
    }

    public override void Start()
    {
        base.Start();

        inputField = UIPicker.Instance.GetInputField();
        inputField.gameObject.SetActive(false);
        if (inputField == null)
        {
            feedbackBox.text = "Need to set InputField in UI";
            return;
        }
        if (!SanityCheckAccessConfiguration())
        {
            return;
        }

    }

    public override void Update()
    {
        if (spawnedObject != null)
        {
            AttachText(inputField.text);
        }
        base.Update();
    }

    protected override bool IsPlacingObject()
    {
        return currentAppState == AppState.CreateLocalAnchor;
    }

    protected override async Task OnSaveCloudAnchorSuccessfulAsync()
    {
        await base.OnSaveCloudAnchorSuccessfulAsync();
        localAnchorIds.Add(currentCloudAnchor.Identifier);
        await anchorAPI.StoreAnchorKey(currentCloudAnchor.Identifier);
        if (inputField.text != "")
        {
            try {
                await anchorAPI.StoreAnchorText(currentCloudAnchor.Identifier, inputField.text);
            } catch(Exception e) {
                feedbackBox.text = e.Message;
            }
        }
        Pose anchorPose = Pose.identity;
        anchorPose = currentCloudAnchor.GetPose();
        SpawnOrMoveCurrentAnchoredObject(anchorPose.position, anchorPose.rotation);
        currentAppState = AppState.StopSession;
    }

    protected override void OnSaveCloudAnchorFailed(Exception exception)
    {
        base.OnSaveCloudAnchorFailed(exception);
    }

    public async override Task AdvanceDemoAsync()
    {
        switch (currentAppState)
        {
            case AppState.CreateSession:
                feedbackBox.text = "Loading...";
                if (CloudManager.Session == null)
                {
                    await CloudManager.CreateSessionAsync();
                }
                currentCloudAnchor = null;
                currentAppState = AppState.StartSession;
                await AdvanceDemoAsync();
                break;
            case AppState.StartSession:
                await CloudManager.StartSessionAsync();
                currentAppState = AppState.CreateLocalAnchor;
                await AdvanceDemoAsync();
                break;
            case AppState.CreateLocalAnchor:
                feedbackBox.text = "";
                if (spawnedObject != null)
                {
                    inputField.gameObject.SetActive(true);
                    currentAppState = AppState.SaveCloudAnchor;
                }
                break;
            case AppState.SaveCloudAnchor:
                currentAppState = AppState.SavingCloudAnchor;
                await SaveCurrentObjectAnchorToCloudAsync();
                inputField.gameObject.SetActive(false);
                feedbackBox.text = "Anchor saved!";
                break;
            case AppState.StopSession:
                CloudManager.StopSession();
                CleanupSpawnedObjects();
                await CloudManager.ResetSessionAsync();
                ReturnToLauncher();
                break;
            default:
                break;
        }
    }
}
