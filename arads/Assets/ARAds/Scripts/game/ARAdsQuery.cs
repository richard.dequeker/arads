﻿using Microsoft.Azure.SpatialAnchors;
using Microsoft.Azure.SpatialAnchors.Unity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;


public class ARAdsQuery : ARAdsBase
{
    internal enum AppState
    {
        CreateSession = 0,
        StartSession,
        LookForAnchor,
        LookingForAnchor,
        DeleteFoundAnchor,
        StopSessionForQuery,
    }

    private AppState _currentAppState = AppState.CreateSession;

    AppState currentAppState
    {
        get
        {
            return _currentAppState;
        }
        set
        {
            if (_currentAppState != value)
            {
                _currentAppState = value;
            }
        }
    }

    public override void Start()
    {
        base.Start();
        if (!SanityCheckAccessConfiguration())
        {
            return;
        }
    }

    protected override void OnCloudAnchorLocated(AnchorLocatedEventArgs args)
    {
        base.OnCloudAnchorLocated(args);

        if (args.Status == LocateAnchorStatus.Located)
        {
            CloudSpatialAnchor locatedAnchor = args.Anchor;

            UnityDispatcher.InvokeOnAppThread(async () =>
            {
                Pose anchorPose = Pose.identity;
                anchorPose = locatedAnchor.GetPose();
                GameObject newAnchor = SpawnNewAnchoredObject(anchorPose.position, anchorPose.rotation);
                try
                {
                    string anchorText = await anchorAPI.RetrieveAnchorText(locatedAnchor.Identifier);
                    feedbackBox.text = anchorText;
                    TextMeshPro textMeshFront = newAnchor.transform.GetChild(0).GetComponent<TextMeshPro>();
                    textMeshFront.text = anchorText;
                    TextMeshPro textMeshBack = newAnchor.transform.GetChild(1).GetComponent<TextMeshPro>();
                    textMeshBack.text = anchorText;
                } catch (Exception e)
                {
                    feedbackBox.text = "Failed to load text";
                }
                spawnedObjects.Add(newAnchor);
                currentAppState = AppState.StopSessionForQuery;
            });
        }
    }

    protected override bool IsPlacingObject()
    {
        return false;
    }

    public override void Update()
    {
        base.Update();
    }

    protected override void OnSaveCloudAnchorFailed(Exception exception)
    {
        base.OnSaveCloudAnchorFailed(exception);
    }

    public async override Task AdvanceDemoAsync()
    {
        switch (currentAppState)
        {
            case AppState.CreateSession:
                feedbackBox.text = "Loading...";
                await ConfigureSession();
                currentAppState = AppState.StartSession;
                await AdvanceDemoAsync();
                break;
            case AppState.StartSession:
                await CloudManager.StartSessionAsync();
                currentAppState = AppState.LookForAnchor;
                await AdvanceDemoAsync();
                break;
            case AppState.LookForAnchor:
                currentAppState = AppState.LookingForAnchor;
                currentWatcher = CreateWatcher();
                await AdvanceDemoAsync();
                break;
            case AppState.LookingForAnchor:
                feedbackBox.text = "";
                break;
            case AppState.StopSessionForQuery:
                CloudManager.StopSession();
                currentWatcher = null;
                CleanupSpawnedObjects();
                ReturnToLauncher();
                break;
            default:
                break;
        }
    }

    private async Task ConfigureSession()
    {
        List<string> anchorIdsToLocate = await anchorAPI.RetrieveAllAnchorKeys();
        SetAnchorIdsToLocate(anchorIdsToLocate);
    }
}
