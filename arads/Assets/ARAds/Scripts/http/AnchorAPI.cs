﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;
using System.Collections.Generic;
using Newtonsoft.Json;

public class AnchorAPI
{
    private string baseAddress = "";

    private List<string> anchorkeys = new List<string>();

    public List<string> AnchorKeys
    {
        get
        {
            lock (anchorkeys)
            {
                return new List<string>(anchorkeys);
            }
        }
    }

    public void WatchKeys(string exchangerUrl)
    {
        baseAddress = exchangerUrl;
        Task.Factory.StartNew(async () =>
            {
                string previousKey = string.Empty;
                while (true)
                {
                    string currentKey = await RetrieveLastAnchorKey();
                    if (!string.IsNullOrWhiteSpace(currentKey) && currentKey != previousKey)
                    {
                        Debug.Log("Found key " + currentKey);
                        lock (anchorkeys)
                        {
                            anchorkeys.Add(currentKey);
                        }
                        previousKey = currentKey;
                    }
                    await Task.Delay(500);
                }
            }, TaskCreationOptions.LongRunning);
    }

    // GET api/anchors/:id
    public async Task<string> RetrieveAnchorText(string anchorId)
    {
        try
        {
            HttpClient client = new HttpClient();
            return await client.GetStringAsync(baseAddress + "/" + anchorId);
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            Debug.LogError($"Failed to retrieve anchor text for anchor number: {anchorId}.");
            return null;
        }
    }

    public async Task<string> RetrieveLastAnchorKey()
    {
        try
        {
            HttpClient client = new HttpClient();
            return await client.GetStringAsync(baseAddress + "/last");
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            Debug.LogError("Failed to retrieve last anchor key.");
            return null;
        }
    }

    public async Task<List<string>> RetrieveAllAnchorKeys()
    {
        try
        {
            HttpClient client = new HttpClient();
            string result = await client.GetStringAsync(baseAddress);
            List<string> res = JsonConvert.DeserializeObject<List<string>>(result);
            return res;
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            Debug.LogError("Failed to retrieve anchors key.");
            return null;
        }
    }

    internal async Task<string> StoreAnchorKey(string anchorKey)
    {
        try
        {
            HttpClient client = new HttpClient();
            var response = await client.PostAsync(baseAddress, new StringContent(anchorKey));
            if (response.IsSuccessStatusCode)
            {
              return await response.Content.ReadAsStringAsync();
            }
            else
            {
                Debug.LogError($"Failed to store the anchor key: {response.StatusCode} {response.ReasonPhrase}.");
            }

            Debug.LogError($"Failed to store the anchor key: {anchorKey}.");
            return "";
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            Debug.LogError($"Failed to store the anchor key: {anchorKey}.");
            return "";
        }
    }


    internal async Task<string> StoreAnchorText(string anchorKey, string anchorText)
    {
        if (string.IsNullOrWhiteSpace(anchorKey))
        {
            return "";
        }

        try
        {
            HttpClient client = new HttpClient();
            var response = await client.PostAsync(baseAddress + "/" + anchorText, new StringContent(anchorText));
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                Debug.LogError($"Failed to store the anchor key: {response.StatusCode} {response.ReasonPhrase}.");
            }

            Debug.LogError($"Failed to store the anchor key: {anchorKey}.");
            return "";
        }
        catch (Exception ex)
        {
            Debug.LogException(ex);
            Debug.LogError($"Failed to store the anchor key: {anchorKey}.");
            return "";
        }
    }
}
