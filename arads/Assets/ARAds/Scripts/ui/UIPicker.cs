using UnityEngine;
using UnityEngine.UI;


public class UIPicker : MonoBehaviour
{
    private static UIPicker _Instance;
    public static UIPicker Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = FindObjectOfType<UIPicker>();
            }

            return _Instance;
        }
    }

    public GameObject MobileUI;

    void Awake()
    {
        MobileUI.SetActive(true);
    }

    public Text GetFeedbackText()
    {
        GameObject sourceTree = null;

        sourceTree = MobileUI;

        Debug.Log(sourceTree.transform.childCount);
        int childCount = sourceTree.transform.childCount;
        for (int index = 0; index < childCount; index++)
        {
            GameObject child = sourceTree.transform.GetChild(index).gameObject;
            Text t = child.GetComponent<Text>();
            if (t != null)
            {
                return t;
            }

        }

        Debug.LogError("Did not find feedback text control.");
        return null;
    }

    public Button GetButton()
    {
        return MobileUI.GetComponentInChildren<Button>(true);
    }

    public InputField GetInputField()
    {
        return MobileUI.GetComponentInChildren<InputField>(true);
    }
}
