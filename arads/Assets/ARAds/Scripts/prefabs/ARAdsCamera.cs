using UnityEngine;


public class ARAdsCamera : MonoBehaviour
{
    public GameObject ARFoundationCameraTree;

    public GameObject EditorCameraTree;

    void Awake()
    {
        GameObject targetCamera = EditorCameraTree;
        targetCamera = ARFoundationCameraTree;
        Instantiate(targetCamera);
    }
}
