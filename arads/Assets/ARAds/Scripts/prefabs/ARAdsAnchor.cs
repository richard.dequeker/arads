﻿using UnityEngine;

public class ARAdsAnchor : MonoBehaviour
{

    float rotationY = 0;
    void Update()
    {
        rotationY += Time.deltaTime * 20;
        transform.rotation = Quaternion.Euler(0, rotationY, 0);
    }
}
